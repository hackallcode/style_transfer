# How to transfer images

Result will be saved in results folder. Weights of model will be in models folder.
```shell script
python main.py base_image_path style_image_path
```

# Links
* [keras](https://keras.io/)
* [deep-learning-models](https://github.com/fchollet/deep-learning-models)
