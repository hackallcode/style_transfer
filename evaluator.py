import numpy as np
from keras import backend as K


class Evaluator:
    def __init__(self, img_rows, img_cols, f_outputs):
        self.img_rows = img_rows
        self.img_cols = img_cols
        self.f_outputs = f_outputs
        self.loss_value = None
        self.grads_value = None

    def eval_loss_and_grads(self, img):
        if K.image_data_format() == 'channels_first':
            img = img.reshape((1, 3, self.img_rows, self.img_cols))
        else:
            img = img.reshape((1, self.img_rows, self.img_cols, 3))
        outs = self.f_outputs([img])
        loss_value = outs[0]
        if len(outs[1:]) == 1:
            grad_values = outs[1].flatten().astype('float64')
        else:
            grad_values = np.array(outs[1:]).flatten().astype('float64')
        return loss_value, grad_values

    def loss(self, img):
        assert self.loss_value is None
        loss_value, grad_values = self.eval_loss_and_grads(img)
        self.loss_value = loss_value
        self.grads_value = grad_values
        return self.loss_value

    def grads(self, img):
        assert self.loss_value is not None
        grad_values = np.copy(self.grads_value)
        self.loss_value = None
        self.grads_value = None
        return grad_values
