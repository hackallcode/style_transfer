import argparse
import os
import time

from keras.preprocessing.image import save_img

from transfer import Transfer


def main():
    parser = argparse.ArgumentParser(description='Neural style transfer with Keras.')
    parser.add_argument(
        'base_image_path', metavar='base', type=str,
        help='Path to the image to transform.'
    )
    parser.add_argument(
        'style_image_path', metavar='ref', type=str,
        help='Path to the style reference image.'
    )
    parser.add_argument(
        '--iter', type=int, default=10, required=False,
        help='Number of iterations to run.'
    )
    parser.add_argument(
        '--scale', type=float, default=400, required=False,
        help='Scale of result image. If > 1 then height in pixels. If <= 1 then scale of original size.'
    )
    parser.add_argument(
        '--content_weight', type=float, default=0.025, required=False,
        help='Content weight.'
    )
    parser.add_argument(
        '--style_weight', type=float, default=1.0, required=False,
        help='Style weight.'
    )
    parser.add_argument(
        '--tv_weight', type=float, default=1.0, required=False,
        help='Total Variation weight.'
    )
    args = parser.parse_args()

    base_image_name = os.path.splitext(os.path.basename(args.base_image_path))[0]
    style_image_name = os.path.splitext(os.path.basename(args.style_image_path))[0]

    result_dir = f'results/{base_image_name}_{style_image_name}'
    os.makedirs(f'{result_dir}', exist_ok=True)
    result_file = f'{result_dir}/result_at_iteration_%d.png'

    transfer = Transfer(
        base_image_path=args.base_image_path,
        style_image_path=args.style_image_path,
        total_variation_weight=args.tv_weight,
        style_weight=args.style_weight,
        content_weight=args.content_weight,
        scale=args.scale,
    )

    for i in range(args.iter):
        print('Start of iteration', i)
        start_time = time.time()
        img = transfer.iteration()
        filename = result_file % i
        save_img(filename, img)
        end_time = time.time()
        print('Image saved as', filename)
        print('Iteration %d completed in %ds' % (i, end_time - start_time))


if __name__ == '__main__':
    main()
