import numpy as np
from keras import backend as K
from keras.preprocessing.image import load_img, img_to_array
from scipy.optimize import fmin_l_bfgs_b

from evaluator import Evaluator
from model import get_vgg19_model, preprocess_input


class Transfer:
    def preprocess_image(self, image_path):
        img = load_img(image_path, target_size=(self.img_rows, self.img_cols))
        img = img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = preprocess_input(img)
        return img

    def content_loss(self, base, combination):
        return K.sum(K.square(combination - base))

    def gram_matrix(self, img):
        assert K.ndim(img) == 3
        if K.image_data_format() == 'channels_first':
            features = K.batch_flatten(img)
        else:
            features = K.batch_flatten(K.permute_dimensions(img, (2, 0, 1)))
        gram = K.dot(features, K.transpose(features))
        return gram

    def style_loss(self, style, combination):
        assert K.ndim(style) == 3
        assert K.ndim(combination) == 3
        s = self.gram_matrix(style)
        c = self.gram_matrix(combination)
        channels = 3
        size = self.img_rows * self.img_cols
        return K.sum(K.square(s - c)) / (4.0 * (channels ** 2) * (size ** 2))

    def total_variation_loss(self, img):
        assert K.ndim(img) == 4
        if K.image_data_format() == 'channels_first':
            a = K.square(
                img[:, :, :self.img_rows - 1, :self.img_cols - 1] - img[:, :, 1:, :self.img_cols - 1])
            b = K.square(
                img[:, :, :self.img_rows - 1, :self.img_cols - 1] - img[:, :, :self.img_rows - 1, 1:])
        else:
            a = K.square(
                img[:, :self.img_rows - 1, :self.img_cols - 1, :] - img[:, 1:, :self.img_cols - 1, :])
            b = K.square(
                img[:, :self.img_rows - 1, :self.img_cols - 1, :] - img[:, :self.img_rows - 1, 1:, :])
        return K.sum(K.pow(a + b, 1.25))

    def __init__(self, base_image_path, style_image_path, total_variation_weight, style_weight, content_weight, scale):
        # dimensions of the generated picture.
        width, height = load_img(base_image_path).size
        self.img_rows = scale if scale > 1 else int(scale * height)
        self.img_cols = int(width * self.img_rows / height)

        # get tens or representations of our images
        base_image = K.variable(self.preprocess_image(base_image_path))
        style_reference_image = K.variable(self.preprocess_image(style_image_path))

        # this will contain our generated image
        if K.image_data_format() == 'channels_first':
            combination_image = K.placeholder((1, 3, self.img_rows, self.img_cols))
        else:
            combination_image = K.placeholder((1, self.img_rows, self.img_cols, 3))

        # combine the 3 images into a single Keras tensor
        input_tensor = K.concatenate([base_image, style_reference_image, combination_image], axis=0)

        # build the VGG19 network with our 3 images as input
        # the model will be loaded with pre-trained ImageNet weights
        model = get_vgg19_model(input_tensor=input_tensor, weights='imagenet', include_top=False)
        print('Model loaded.')

        # get the symbolic outputs of each "key" layer (we gave them unique names).
        outputs_dict = dict([(layer.name, layer.output) for layer in model.layers])

        # combine these loss functions into a single scalar
        loss = K.variable(0.0)
        layer_features = outputs_dict['block5_conv2']
        base_image_features = layer_features[0, :, :, :]
        combination_features = layer_features[2, :, :, :]
        loss = loss + content_weight * self.content_loss(base_image_features, combination_features)

        feature_layers = ['block1_conv1', 'block2_conv1', 'block3_conv1', 'block4_conv1', 'block5_conv1']
        for layer_name in feature_layers:
            layer_features = outputs_dict[layer_name]
            style_reference_features = layer_features[1, :, :, :]
            combination_features = layer_features[2, :, :, :]
            sl = self.style_loss(style_reference_features, combination_features)
            loss = loss + (style_weight / len(feature_layers)) * sl
        loss = loss + total_variation_weight * self.total_variation_loss(combination_image)

        # get the gradients of the generated image wrt the loss
        grads = K.gradients(loss, combination_image)

        outputs = [loss]
        if isinstance(grads, (list, tuple)):
            outputs += grads
        else:
            outputs.append(grads)

        self.evaluator = Evaluator(self.img_rows, self.img_cols, K.function([combination_image], outputs))
        self.img = self.preprocess_image(base_image_path)

    def deprocess_image(self, img):
        if K.image_data_format() == 'channels_first':
            img = img.reshape((3, self.img_rows, self.img_cols))
            img = img.transpose((1, 2, 0))
        else:
            img = img.reshape((self.img_rows, self.img_cols, 3))
        # Remove zero-center by mean pixel
        img[:, :, 0] += 103.939
        img[:, :, 1] += 116.779
        img[:, :, 2] += 123.68
        # 'BGR'->'RGB'
        img = img[:, :, ::-1]
        img = np.clip(img, 0, 255).astype('uint8')
        return img

    def iteration(self):
        self.img, min_val, _ = fmin_l_bfgs_b(
            self.evaluator.loss,
            self.img.flatten(),
            fprime=self.evaluator.grads,
            maxfun=20
        )
        print('Current loss value:', min_val)
        return self.deprocess_image(self.img.copy())
